package com.pashine.pashineyunba;


public class Constants {
    public static final String ACTION_PING = "com.pashine.pashineyunba.PING";
    public static final String newProtocolMessageTopicPrefix = ",ybl/";
    public static final String gpsResponseTopic = ",ybls";
    public static final String lockpingTopic = ",yhb";
    public static final String lockRequestTopic = ",ybls";

    public static final int DEFAULT_PING_INTERVAL = 200;

    public static final String SP_PING_INTERVAL_KEY = "ping_interval_second";
    public static final String SP_ENABLE_GPS_KEY = "enable_gps";
    public static final String SP_SESSION_ID_KEY = "sid";

    public static final int ENABLE_GPS = 1;
    public static final int DISABLE_GPS = 0;

    public interface NewProtocolCmdAction {
        int pre_unlock = 1;
        int do_unlock = 2;
        int lock_request = 6;
        int confirm_lock = 8;
        int set_alive_interval = 10;
        int enable_report_gps = 11;
        int gps_request = 12;
        int gps_response = 13;
        int upgrade = 14;
    }
}
