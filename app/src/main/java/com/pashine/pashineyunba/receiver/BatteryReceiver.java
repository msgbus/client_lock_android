package com.pashine.pashineyunba.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.BatteryManager;
import android.util.Log;
import android.widget.Toast;

import com.pashine.pashineyunba.DemoUtil;

import static android.content.Intent.ACTION_BATTERY_CHANGED;

public class BatteryReceiver extends BroadcastReceiver {
    private String TAG="BatteryReceiver";
    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        if (intent.getAction().equals(Intent.ACTION_BATTERY_CHANGED)) {
            int current=intent.getExtras().getInt("level");//获得当前电量
            int total=intent.getExtras().getInt("scale");//获得总电量
            DemoUtil.batterypercent=current*100/total;
            int chargePlug = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
            if (chargePlug==BatteryManager.BATTERY_PLUGGED_USB || chargePlug==BatteryManager.BATTERY_PLUGGED_AC){
                DemoUtil.BatteryACORUSBPLUG=true;
            }else {
                DemoUtil.BatteryACORUSBPLUG=false;
            }
            int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
            switch (status) {
                case BatteryManager.BATTERY_STATUS_CHARGING:
                    // 正在充电
                    DemoUtil.BatteryStatus=true;
                    Log.d("正在充电","");
                    break;
                case BatteryManager.BATTERY_STATUS_DISCHARGING:
                    DemoUtil.BatteryStatus=false;
                    Log.d("DISCHARGING","");
                    break;
                case BatteryManager.BATTERY_STATUS_FULL:
                    // 充满
                    DemoUtil.BatteryStatus=false;
                    Log.d("充满","");
                    break;
                case BatteryManager.BATTERY_STATUS_NOT_CHARGING:
                    // 没有充电
                    DemoUtil.BatteryStatus=false;
                    Log.d("没有充电","");
                    break;
                case BatteryManager.BATTERY_STATUS_UNKNOWN:
                    // 未知状态
                    DemoUtil.BatteryStatus=false;
                    Log.d("未知状态","");
                    break;
                default:
                    break;
            }
            Log.d("现在的电量是"+DemoUtil.batterypercent+"%。","充电:"+DemoUtil.BatteryStatus);
           // Toast.makeText(context,"电量:"+DemoUtil.batterypercent+"%。" +"充电:"+DemoUtil.BatteryStatus,Toast.LENGTH_LONG).show();
        }
    }
}
