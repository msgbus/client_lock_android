package com.pashine.pashineyunba.receiver;


import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.pashine.pashineyunba.Constants;
import com.pashine.pashineyunba.SharePrefsHelper;

import java.util.Calendar;

import static com.pashine.pashineyunba.utils.PingManager.reportHeartbeat;

public class PingReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String theAction = intent.getAction();

        if (Constants.ACTION_PING.equals(theAction)) {
            PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(System.currentTimeMillis());
            int aliveInterval = SharePrefsHelper.getInt(context, Constants.SP_PING_INTERVAL_KEY, Constants.DEFAULT_PING_INTERVAL);
            calendar.add(Calendar.SECOND, aliveInterval);


            AlarmManager am = (AlarmManager) context
                    .getSystemService(Context.ALARM_SERVICE);
            am.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), sender);
            reportHeartbeat(context);
        }
    }
}
