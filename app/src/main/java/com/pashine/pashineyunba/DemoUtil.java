package com.pashine.pashineyunba;

import java.util.List;
import java.util.Random;

import android.app.ActivityManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.telephony.NeighboringCellInfo;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;
import android.widget.Toast;

import com.pashine.pashineyunba.location.Gps;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.yunba.android.manager.YunBaManager;
import static com.pashine.pashineyunba.telephone.telephoneCall.getNeighboringCellInfo;

public class DemoUtil {
	public static int batterypercent=0;
	public static boolean BatteryStatus=false;
	public static boolean BatteryACORUSBPLUG=false;
	public static String Report_Topic="9999999999";
	//public static String Receiver_Topic="receiver_topic";
	public static String Receiver_Alias=Report_Topic;
	private static String locksharepre="lockstatus";
	public static void setlockstatus(boolean lock,Context mcontext){
		SharedPreferences mSharedPreferences = mcontext.getSharedPreferences(locksharepre, mcontext.MODE_PRIVATE);
		SharedPreferences.Editor editor = mSharedPreferences.edit();
		editor.putBoolean("lockstatus", lock);
		editor.commit();
	}
	public static boolean getlockstatus(Context mcontext){
		SharedPreferences mSharedPreferences = mcontext.getSharedPreferences(locksharepre, mcontext.MODE_PRIVATE);
		return mSharedPreferences.getBoolean(locksharepre, false);
	}
	public static boolean isEmpty(String s) {
		if (null == s)
			return true;
		if (s.length() == 0)
			return true;
		if (s.trim().length() == 0)
			return true;
		return false;
	}
    public static String Demogetimei(Context context){
			TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(context.TELEPHONY_SERVICE);
			String imei = telephonyManager.getDeviceId();
		    if (imei !=null && !imei.isEmpty()){
				Report_Topic=imei.substring(imei.length()-10);
				Receiver_Alias=Report_Topic;
			}
			return imei;
	}
	public static boolean showNotifation(Context context, String topic,
			String msg) {
		try {
			 Uri alarmSound = RingtoneManager
			 .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			
			 long[] pattern = { 500, 500, 500 };
			 NotificationCompat.Builder mBuilder = new
			 NotificationCompat.Builder(
			 context).setSmallIcon(R.drawable.ic_launcher)
			 .setContentTitle(topic).setContentText(msg)
			 .setSound(alarmSound).setVibrate(pattern).setAutoCancel(true);
			 // Creates an explicit intent for an Activity in your app
			 Intent resultIntent = new Intent(context,
			 YunBaTabActivity.class);
			
			 if (!DemoUtil.isEmpty(topic))
			 resultIntent.putExtra(YunBaManager.MQTT_TOPIC, topic);
			 if (!DemoUtil.isEmpty(msg))
			 resultIntent.putExtra(YunBaManager.MQTT_MSG, msg);
			 // The stack builder object will contain an artificial back stack
			 // for the
			 // started Activity.
			 // This ensures that navigating backward from the Activity leads
			 
			 // of
			 // your application to the Home screen.
			 TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
			 // Adds the back stack for the Intent (but not the Intent itself)
			 stackBuilder.addParentStack(MainActivity.class);
			
			 // Adds the Intent that starts the Activity to the top of the
			 
			 stackBuilder.addNextIntent(resultIntent);
			 PendingIntent resultPendingIntent =
			 stackBuilder.getPendingIntent(
			 0, PendingIntent.FLAG_UPDATE_CURRENT);
			
			 mBuilder.setContentIntent(resultPendingIntent);
			 NotificationManager mNotificationManager = (NotificationManager)
			 context.getSystemService(Context.NOTIFICATION_SERVICE);
			 // mId allows you to update the notification later on.
			 Random r = new Random();
			 mNotificationManager.notify(r.nextInt(), mBuilder.build());

//			NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
//					context).setSmallIcon(R.drawable.ic_launcher) // notification
//																	// icon
//					.setContentTitle(topic) // title for notification
//					.setContentText(msg) // message for notification
//					.setAutoCancel(true); // clear notification after click
//			Intent intent = new Intent(context, YunBaTabActivity.class);
//			
//			 if (!DemoUtil.isEmpty(topic))
//				 intent.putExtra(YunBaManager.MQTT_TOPIC, topic);
//			 if (!DemoUtil.isEmpty(msg))
//				 intent.putExtra(YunBaManager.MQTT_MSG, msg);
//			PendingIntent pi = PendingIntent.getActivity(context, 0, intent,
//					Intent.FLAG_ACTIVITY_NEW_TASK);
//			mBuilder.setContentIntent(pi);
//			NotificationManager mNotificationManager = (NotificationManager) context
//					.getSystemService(Context.NOTIFICATION_SERVICE);
//			mNotificationManager.notify(0, mBuilder.build());
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public static void showToast(final String toast, final Context context) {
		// if (!isAppOnForeground(context)) return;
		// new Thread(new Runnable() {
		//
		// @Override
		// public void run() {
		// Looper.prepare();
		// Toast.makeText(context, toast, Toast.LENGTH_SHORT).show();
		// Looper.loop();
		// }
		// }).start();
	}

	public static boolean isAppOnForeground(Context context) {
		ActivityManager activityManager = (ActivityManager) context
				.getSystemService(Context.ACTIVITY_SERVICE);
		// Returns a list of application processes that are running on the
		// device
		List<RunningAppProcessInfo> appProcesses = activityManager
				.getRunningAppProcesses();
		if (appProcesses == null)
			return false;
		for (RunningAppProcessInfo appProcess : appProcesses) {
			// importance:
			// The relative importance level that the system places
			// on this process.
			// May be one of IMPORTANCE_FOREGROUND, IMPORTANCE_VISIBLE,
			// IMPORTANCE_SERVICE, IMPORTANCE_BACKGROUND, or IMPORTANCE_EMPTY.
			// These constants are numbered so that "more important" values are
			// always smaller than "less important" values.
			// processName:
			// The name of the process that this object is associated with.
			if (appProcess.processName.equals(context.getPackageName())
					&& appProcess.importance == RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
				return true;
			}
		}
		return false;
	}

	public static <T> String join(T[] array, String cement) {
		StringBuilder builder = new StringBuilder();

		if (array == null || array.length == 0) {
			return null;
		}
		for (T t : array) {
			builder.append(t).append(cement);
		}

		builder.delete(builder.length() - cement.length(), builder.length());

		return builder.toString();
	}

	public static boolean isNetworkEnabled(Context context) {
		ConnectivityManager conn = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = conn.getActiveNetworkInfo();
		return (info != null && info.isConnected());
	}

	public static String getImei(Context context, String imei) {
		TelephonyManager telephonyManager = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);
		imei = telephonyManager.getDeviceId();
		return imei;
	}

	public static String getAppKey(Context context) {
		Bundle metaData = null;
		String appKey = null;
		try {
			ApplicationInfo ai = context.getPackageManager()
					.getApplicationInfo(context.getPackageName(),
							PackageManager.GET_META_DATA);
			if (null != ai) {
				metaData = ai.metaData;
			}
			if (null != metaData) {
				appKey = metaData.getString("YUNBA_APPKEY");
				if ((null == appKey) || appKey.length() != 24) {
					appKey = "Error";
				}
			}
		} catch (NameNotFoundException e) {

		}
		return appKey;
	}
	private static JSONArray getsim(Context mcontext){
			TelephonyManager mTelephonyManager = (TelephonyManager) mcontext.getSystemService(Context.TELEPHONY_SERVICE);
		     int simstatus=mTelephonyManager.getSimState();
		    if (simstatus==TelephonyManager.SIM_STATE_ABSENT || simstatus==TelephonyManager.SIM_STATE_UNKNOWN){
				return null;
			}
			// 返回值MCC + MNC
			String operator = mTelephonyManager.getNetworkOperator();
		    int mcc =460;
		    int mnc =4;
		    if(operator!=null && !operator.isEmpty()) {
				mcc = Integer.parseInt(operator.substring(0, 3));
				mnc = Integer.parseInt(operator.substring(3));
			}
			// 中国移动和中国联通获取LAC、CID的方式
			GsmCellLocation location = (GsmCellLocation) mTelephonyManager.getCellLocation();
			int lac = location.getLac();
			int cellId = location.getCid();
			Log.i("TAG", " MCC = " + mcc + "\t MNC = " + mnc + "\t LAC = " + lac + "\t CID = " + cellId);
			// 中国电信获取LAC、CID的方式
                /*CdmaCellLocation location1 = (CdmaCellLocation) mTelephonyManager.getCellLocation();
                lac = location1.getNetworkId();
                cellId = location1.getBaseStationId();
                cellId /= 16;*/
			// 获取邻区基站信息
			List<NeighboringCellInfo> infos = getNeighboringCellInfo();//mTelephonyManager.getNeighboringCellInfo();
			StringBuffer sb = new StringBuffer("总数 : " + infos.size() + "\n");
				JSONArray arrycell=new JSONArray();
			     int num=0;
				for (NeighboringCellInfo info1 : infos) { // 根据邻区总数进行循环
					JSONObject cellopts = new JSONObject();
					try {
						cellopts.put("mcc", mcc);
						cellopts.put("mnc", mnc);
						cellopts.put("lac", info1.getLac());
						cellopts.put("ci", info1.getCid());
						cellopts.put("sig", (-113 + 2 * info1.getRssi()));
						arrycell.put(num,cellopts);
						num++;
						//arrycell.put(1,cellopts);
					}catch (JSONException ee){
						Log.d("getsim","JSONException");
					}
					sb.append(" LAC : " + info1.getLac()); // 取出当前邻区的LAC
					sb.append(" CID : " + info1.getCid()); // 取出当前邻区的CID
					sb.append(" BSSS : " + (-113 + 2 * info1.getRssi()) + "\n"); // 获取邻区基站信号强度
				}
		/*JSONObject cellopts = new JSONObject();
		try {
			cellopts.put("mcc", mcc);
			cellopts.put("mnc", mnc);
			cellopts.put("lac", 222);
			cellopts.put("ci", 44444);
			cellopts.put("sig", 2222);
			arrycell.put(0,cellopts);
			arrycell.put(1,cellopts);
		}catch (JSONException ee){
			Log.d("getsim","JSONException");
		}*/
		Toast.makeText(mcontext,"获取邻区基站信息:"+sb.toString(),Toast.LENGTH_LONG).show();
			Log.i("TAG", " 获取邻区基站信息:" + sb.toString());
		return arrycell;
	}
	public static JSONObject Report_data(Context mcontext, Gps gpsclass){
		///getCellInfoArray(mcontext);
		String gpsnmea=gpsclass.getstringLocattion();
		gpsclass.setstringLocattion("");
		JSONArray simarray=getsim(mcontext);
		JSONObject lockopts = new JSONObject();
		try {
			lockopts.put("lock", getlockstatus(mcontext));
			lockopts.put("gps", gpsnmea);
			lockopts.put("battery", DemoUtil.batterypercent);
			lockopts.put("charge", DemoUtil.BatteryStatus);
			lockopts.put("cell",simarray);
		}catch (JSONException ee){
			Log.d("Report_data","JSONException");
		}
		Log.d("DemoUtil report data:",lockopts.toString());
		return lockopts;
	}
}
