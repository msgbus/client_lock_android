package com.pashine.pashineyunba;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.media.ToneGenerator;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager.WakeLock;
import android.os.Vibrator;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.pashine.pashineyunba.location.Gps;
import com.pashine.pashineyunba.receiver.BatteryReceiver;
import com.pashine.pashineyunba.utils.ControlGpio;
import com.pashine.pashineyunba.utils.ControlLeds;
import com.pashine.pashineyunba.utils.NewProtocolUtil;
import com.pashine.pashineyunba.utils.PingManager;

import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.yunba.android.manager.YunBaManager;

import static com.pashine.pashineyunba.utils.ControlLeds.NetworkConnectsuccessLeds;
import static com.pashine.pashineyunba.utils.ControlLeds.TurnOnControlLeds;

public class MainActivity extends Activity  implements android.view.View.OnClickListener {

	private final static String TAG = "MainActivity";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	
		super.onCreate(savedInstanceState);
		System.err.println("onCreate MainActivity");
		Thread ledsthread=new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				TurnOnControlLeds();
			}
		});
		ledsthread.start();
		setContentView(R.layout.activity_main);
		///setdefaultBroker("http://rest.yunba.io");///set server addr
		setfilter();///battery
		initUI();
	///	InitTimer();////定时report数据
		registerMessageReceiver();  // used for receive msg
		/*if (null == wakeLock) {
			PowerManager pm = (PowerManager)this.getSystemService(Context.POWER_SERVICE);
			wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK|PowerManager.ON_AFTER_RELEASE ,  this.getClass().getCanonicalName());
			{
				wakeLock.setReferenceCounted(false);
				wakeLock.acquire();
				if(wakeLock.isHeld()) {
					Log.i("main", "wakeLock Held");
				}
			}
		}*/
		ControlGpio.getlockstatus();
		// 开始进行心跳
		PingManager.schedulePing(getApplicationContext());
	}
	
	private Button publish;
	private Button sub;
	private Button unsubscibe;
	private Button publish_sec;
	private Button btn_broker;
	
	private Button testsystembt;
	private Button get_alias;
	private Button set_alias;
	private EditText pub_topic;
	private EditText pub_msg;
	private EditText sub_topic;
	private EditText alias_of_getset;
	private EditText txt_broker;
	
	private TextView pkgName;
	private TextView appKey;
	public static TextView msg_show;
	public static ScrollView scroll;
	public static boolean isForeground = false;
	public final static String MESSAGE_RECEIVED_ACTION = "io.yunba.example.msg_received_action";
	public final static String CONNECT_STATUS = "connect_status";
	private BatteryReceiver mBatteryReceiver;
	private Gps locationgps;
	private Vibrator mVibrator = null;
	//private TimerTask task_report;
	//private Handler handler_report;
	//private Timer timer_report = new Timer();
	private JSONObject mJSONObject;
	public final static String LOCK_STATUS_ACTION = "com.pashine.pashineyunba.lock";
	public final static String UNLOCK_STATUS_ACTION = "com.pashine.pashineyunba.unlock";
	private MediaPlayer mediaPlayer=null;
	private WakeLock wakeLock = null;
	private ToneGenerator mToneGenerator;
	private void initUI() {
		publish = (Button)findViewById(R.id.publish);
		testsystembt = (Button)findViewById(R.id.testsystembt);
		sub = (Button)findViewById(R.id.subscribe);
		unsubscibe = (Button)findViewById(R.id.ping);
		publish_sec = (Button)findViewById(R.id.publish_select);
		get_alias = (Button)findViewById(R.id.get_alias);
		set_alias = (Button)findViewById(R.id.set_alias);
		btn_broker = (Button)findViewById(R.id.btn_broker);
		publish.setOnClickListener(this);
		sub.setOnClickListener(this);
		testsystembt.setOnClickListener(this);
		unsubscibe.setOnClickListener(this);
		publish_sec.setOnClickListener(this);
		set_alias.setOnClickListener(this);
		get_alias.setOnClickListener(this);
		btn_broker.setOnClickListener(this);
		alias_of_getset = (EditText)findViewById(R.id.alias_of_getset);
		pub_topic = (EditText)findViewById(R.id.publish_topic);
		pub_msg = (EditText)findViewById(R.id.publish_msg);
		sub_topic = (EditText)findViewById(R.id.sub_topic);
		pkgName = (TextView)findViewById(R.id.tv_pkgname);
		pkgName.setText("AppID：" + getPackageName());
		appKey = (TextView)findViewById(R.id.tv_appkey);
		appKey.setText("AppKey：" + DemoUtil.getAppKey(getApplicationContext()));
		txt_broker = (EditText)findViewById(R.id.txt_broker);
//		msg_show.setMovementMethod(ScrollingMovementMethod.getInstance());
//		msg_show.setBackgroundResource(R.drawable.text_view_border); 
//		msg_show.setMaxLines(300);
		String status = SharePrefsHelper.getString(getApplicationContext(), CONNECT_STATUS, null);
		if(!DemoUtil.isEmpty(status)) {
			setTitleOfApp(status);
		}
		if (!DemoUtil.isNetworkEnabled(getApplicationContext())) {
			setTitleOfApp("YunBa - DisConnected");
		}
	//	initLog();
	//	scroll = (ScrollView) findViewById(R.id.scroller);
	}
    public void DisplayImei(){
		if (pub_topic!=null) {
			pub_topic.setText(DemoUtil.Receiver_Alias);
		}
	}
	/*打开振动*/
	public void startPhoneVibrate(){
		mVibrator = (Vibrator) getSystemService(Service.VIBRATOR_SERVICE);
		mVibrator.vibrate(new long[]{0, 10000, 0, 10000}, 0);
		//根据指定的模式进行震动
		//第一个参数：该数组中第一个元素是等待多长的时间才启动震动，
		//之后将会是开启和关闭震动的持续时间，单位为毫秒
		//第二个参数：重复震动时在pattern中的索引，如果设置为-1则表示不重复震动
	}

	/*关闭振动*/
	public void stopPhoneVibrate(){
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
		if (mVibrator != null ) {
			mVibrator.cancel();
			mVibrator = null;
		}
		//根据指定的模式进行震动
		//第一个参数：该数组中第一个元素是等待多长的时间才启动震动，
		//之后将会是开启和关闭震动的持续时间，单位为毫秒
		//第二个参数：重复震动时在pattern中的索引，如果设置为-1则表示不重复震动
	}
	public void PhoneVibrate(){
		startPhoneVibrate();
		///stopPhoneVibrate();
	}
	///curl -l -H "Content-type: application/json" -X POST -d "{\"method\":\"publish_to_alias\",
	// \"appkey\": \"58da2ee3691ac9095323546f\", \"seckey\":\"sec-dMI7f2i9sufgJLLxDKa5IJM6e1s1nQjt65uGA5mzYmhuraDr\",
	// \"alias\":\"yunba_curl_demo_alias\", \"msg\":\"message from RESTful API by publish to alias\", \"opts\":{\"time_to_live\":20000}}"
	// http://rest.yunba.io:8080
	private void setdefaultBroker(String broker) {
		if(!DemoUtil.isEmpty(broker)){
			YunBaManager.setBroker(getApplicationContext(), "tcp://"+ broker+":8081");////reg.yunba.io
		} else {
			YunBaManager.setBroker(getApplicationContext(), null);
		}

	}
	/*private void InitTimer(){
		handler_report = new Handler() {
			@Override
			public void handleMessage(Message msg)
			{
				GetJsonData();
				super.handleMessage(msg);
			}
		};
		task_report = new TimerTask() {
			@Override
			public void run() {
				Message message = new Message();
				message.what = 1;
				handler_report.sendMessage(message);
			}
		};
		timer_report.schedule(task_report, 0, 1*60*1000);///1s=1000
	}*/
	private void GetJsonData(){
		if (locationgps==null) {
			locationgps = new Gps(this);
		}
		mJSONObject=DemoUtil.Report_data(this,locationgps);
		Report_Data_YunBa();
		///Report_Data_to_Alias_YunBa();
	}
	private void Report_Data_to_Alias_YunBa(){
		final String topic = DemoUtil.Receiver_Alias;////pub_topic.getText().toString().trim();
		final String msg = mJSONObject.toString();///pub_msg.getText().toString().trim();
		///Log.d("start Publish2 msg", "= " + msg + " to topic = " + topic + " reprot data:"+mJSONObject.toString());
		if (TextUtils.isEmpty(topic) || TextUtils.isEmpty(msg)) {
			Toast.makeText(MainActivity.this, "String should not be null", Toast.LENGTH_SHORT).show();
			return;
		}
		addTopic(topic);
		///Log.d("Publish2 msg", "= " + msg + " to topic = " + topic);
		setCostomMsg("Publish2 msg = " + msg + " to topic = " + topic);
		YunBaManager.publish2ToAlias(getApplicationContext(), topic, msg, mJSONObject,
				new IMqttActionListener() {
					@Override
					public void onSuccess(IMqttToken asyncActionToken) {
						String topic = DemoUtil.join(asyncActionToken.getTopics(), ", ");
						String msgLog = "Publish2 succeed : " + topic;
						DemoUtil.showToast(msgLog, getApplicationContext());
					}

					@Override
					public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
						if (exception instanceof MqttException) {
							MqttException ex = (MqttException)exception;
							String msg =  "publish2 failed with error code : " + ex.getReasonCode();
							DemoUtil.showToast(msg, getApplicationContext());
						}
					}
				}
		);
	}
	private void Report_Data_YunBa(){
		final String topic = DemoUtil.Report_Topic;////pub_topic.getText().toString().trim();
		final String msg = mJSONObject.toString();///pub_msg.getText().toString().trim();
		///Log.d("start Publish2 msg", "= " + msg + " to topic = " + topic + " reprot data:"+mJSONObject.toString());
		if (TextUtils.isEmpty(topic) || TextUtils.isEmpty(msg)) {
			Toast.makeText(MainActivity.this, "String should not be null", Toast.LENGTH_SHORT).show();
			return;
		}
		addTopic(topic);
		///Log.d("Publish2 msg", "= " + msg + " to topic = " + topic);
		setCostomMsg("Publish2 msg = " + msg + " to topic = " + topic);
		YunBaManager.publish(getApplicationContext(), topic, msg,
				new IMqttActionListener() {
					@Override
					public void onSuccess(IMqttToken asyncActionToken) {
						String topic = DemoUtil.join(asyncActionToken.getTopics(), ", ");
						String msgLog = "Publish2 succeed : " + topic;
						DemoUtil.showToast(msgLog, getApplicationContext());
					}

					@Override
					public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
						if (exception instanceof MqttException) {
							MqttException ex = (MqttException)exception;
							String msg =  "publish2 failed with error code : " + ex.getReasonCode();
							DemoUtil.showToast(msg, getApplicationContext());
						}
					}
				}
		);
	}
	private void setfilter(){
		mBatteryReceiver=new BatteryReceiver();
		IntentFilter filter2 = new IntentFilter();
		filter2.addAction(Intent.ACTION_BATTERY_CHANGED);
		registerReceiver(mBatteryReceiver,filter2);
	}
	private void initLog() {
		Intent intent = getIntent();
		if (null != intent) {
		    Bundle bundle = intent.getExtras();
		    if (null != bundle) {
		    String topic = bundle.getString(YunBaManager.MQTT_TOPIC);
		    String msg = bundle.getString(YunBaManager.MQTT_MSG);
		   // System.err.println(topic + " ; " + msg);
			StringBuilder showMsg = new StringBuilder();
			showMsg.append("Received msg from server: ").append(YunBaManager.MQTT_TOPIC)
					.append(" = ").append(topic).append(" ")
					.append(YunBaManager.MQTT_MSG).append(" = ").append(msg);
			//System.err.println(showMsg);
			setCostomMsg(showMsg.toString());
		    } else {
		   // 	System.err.println("null bundle");
		    }
	     } else {
	    	 	//System.err.println("null intent");
	     }
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}	



	@Override
	protected void onResume() {
		isForeground = true;
		super.onResume();
		restoreDatas();
		initLog();
//		scroll.post(new Runnable() {
//			public void run() {
//				if (null != scroll) scroll.fullScroll(View.FOCUS_DOWN);
//			}
//		});
	}

	private void restoreDatas() {
		String last_pub = SharePrefsHelper.getString(getApplicationContext(), YunBaManager.LAST_PUB, null);
		if (!DemoUtil.isEmpty(last_pub)) pub_topic.setText(last_pub);
		
		String last_sub = SharePrefsHelper.getString(getApplicationContext(), YunBaManager.LAST_SUB, null);
		if (!DemoUtil.isEmpty(last_sub)) sub_topic.setText(last_sub);
		
		String topicsStr = SharePrefsHelper.getString(getApplicationContext(), YunBaManager.HISTORY_TOPICS, null);
		if (!DemoUtil.isEmpty(topicsStr)) {
			Log.i(TAG, "getHistoryTopics: " + topicsStr);
			String[] topicsArr = topicsStr.split("\\$\\$");
			if (topics.size() == 0) {
				List<String> list = Arrays.asList(topicsArr);
				topics = new ArrayList<String>(list);
			}
		}
		
	}


	@Override
	protected void onPause() {
		isForeground = false;
		super.onPause();
		saveLastFiveTopics();
	}

	private void saveLastFiveTopics() {
		int size = topics.size();
		StringBuilder sb = new StringBuilder();
		for (int i = (topics.size() -1 ); (i >= 0  && i >= size -6) ; i--) {
			sb.append(topics.get(i));
			if (!(i == 0 ||  i == size -6)) {
				sb.append("$$");
			}
		}
		String topicsStr = sb.toString();
		Log.i(TAG, "saveLastFiveTopics: " + topicsStr);
		if (DemoUtil.isEmpty(topicsStr)) return;
		SharePrefsHelper.setString(getApplicationContext(), YunBaManager.HISTORY_TOPICS, topicsStr);
	}

	@Override
	protected void onDestroy() {
		unregisterReceiver(mMessageReceiver);
		unregisterReceiver(mBatteryReceiver);
		CleanAlarm();
		/*if (null != wakeLock) {
			wakeLock.release();
			wakeLock = null;
		}*/
		/*if (timer_report != null) {
			timer_report.cancel();
			timer_report = null;
		}
		if (task_report != null) {
			task_report.cancel();
			task_report = null;
		}*/
		super.onDestroy();
	}
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.ping:
			unsubscribe();
			break;
		case R.id.publish:
			publish();
			break;
		case R.id.testsystembt:
			//Intent testapp = getPackageManager().getLaunchIntentForPackage("lava.com.android.mmitest");
			Intent testapp = new Intent();
			testapp.setClassName("lava.com.android.mmitest","lava.com.android.mmitest.GnMMITest");
			if (testapp!=null) {
				startActivity(testapp);
			}
			break;
		case R.id.subscribe:
			subscribe();
			break;
		case R.id.publish_select:
			showTopic(pub_topic);
			break;	
		case R.id.set_alias:
			setAlias();
			break;		
		case R.id.get_alias:
			getAlias();
			break;	
		case R.id.btn_broker:
			setBroker();
			break;			
		default:
			break;
		}
		
	}

	private void setBroker() {
		String broker = txt_broker.getText().toString().trim();
	    Log.i(TAG, "Broker ip = " +  broker);
		     if(!DemoUtil.isEmpty(broker)){
	    	 YunBaManager.setBroker(getApplicationContext(), "tcp://"+ broker+":1883");
		     } else {
		    	YunBaManager.setBroker(getApplicationContext(), null);
		     }
		
	}

	private void getAlias() {
		setCostomMsg("get alias ");
		YunBaManager.getAlias(getApplicationContext(),new IMqttActionListener() {
			
			@Override
			public void onSuccess(IMqttToken arg) {
				final String alias = arg.getAlias();

				StringBuilder showMsg = new StringBuilder();
				showMsg.append("[Demo] getAlias alias ").append(" = ")
						.append(alias).append(" succeed");
				if(null != alias) {
					MainActivity.this.runOnUiThread(new Runnable() {
						
						@Override
						public void run() {
							alias_of_getset.setText(alias);							
						}
					});
					
				}
				setCostomMsg(showMsg.toString());

			}
			
			@Override
			public void onFailure(IMqttToken arg0, Throwable arg) {
				StringBuilder showMsg = new StringBuilder();
				showMsg.append("[Demo] setAlias alias ").append(" failed");
	   	        setCostomMsg(showMsg.toString());
			}
		});
		
	}

	private void setAlias() {
		final String alias = alias_of_getset.getText().toString().trim();
		if (TextUtils.isEmpty(alias)) {
			Toast.makeText(MainActivity.this, "Alias should not be null", Toast.LENGTH_SHORT).show();
			return;
		}
		setCostomMsg("set alias = " + alias);
		YunBaManager.setAlias(getApplicationContext(), alias, new IMqttActionListener() {
			
			@Override
			public void onSuccess(IMqttToken arg) {
				StringBuilder showMsg = new StringBuilder();
				showMsg.append("[Demo] setAlias alias ")
				.append(" = ").append(alias).append(" succeed");
	   	        setCostomMsg(showMsg.toString());				
			}
			
			@Override
			public void onFailure(IMqttToken arg0, Throwable arg) {
				StringBuilder showMsg = new StringBuilder();
				showMsg.append("[Demo] setAlias alias ")
				.append(" = ").append(alias).append(" failed");
	   	        setCostomMsg(showMsg.toString());
			}
		});
	}

	private void unsubscribe() {
		final String topic = sub_topic.getText().toString().trim();
		if (TextUtils.isEmpty(topic)) {
			Toast.makeText(MainActivity.this, "String should not be null", Toast.LENGTH_SHORT).show();
			return;
		}
		addTopic(topic);
		setCostomMsg("unsubscribe topic = " + topic);
		YunBaManager.unsubscribe(getApplicationContext(), topic, new IMqttActionListener() {
			
			@Override
			public void onSuccess(IMqttToken asyncActionToken) {
				DemoUtil.showToast( "unsubscribe succeed : " + topic, getApplicationContext());
				StringBuilder showMsg = new StringBuilder();
				showMsg.append("[Demo] unsubscribe ").append(YunBaManager.MQTT_TOPIC)
						.append(" = ").append(topic).append(" succeed");
				setCostomMsg(showMsg.toString());
			}
			
			@Override
			public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
				String msg =  "[Demo] unsubscribe topic = "+ topic +" failed : " + exception.getMessage();
				setCostomMsg(msg);
				DemoUtil.showToast(msg, getApplicationContext());
				
				
			}
		});
		SharePrefsHelper.setString(getApplicationContext(), YunBaManager.LAST_SUB, topic);
	}

	private void subscribe() {
		final String topic = DemoUtil.Report_Topic;//sub_topic.getText().toString().trim();
		if (TextUtils.isEmpty(topic)) {
			Toast.makeText(MainActivity.this, "String should not be null", Toast.LENGTH_SHORT).show();
			return;
		}
		addTopic(topic);
		setCostomMsg("Subscribe topic = " + topic);
		YunBaManager.subscribe(getApplicationContext(), topic, new IMqttActionListener() {
			
			@Override
			public void onSuccess(IMqttToken asyncActionToken) {
				DemoUtil.showToast( "Subscribe succeed : " + topic, getApplicationContext());
				StringBuilder showMsg = new StringBuilder();
				showMsg.append("[Demo] subscribe ").append(YunBaManager.MQTT_TOPIC)
						.append(" = ").append(topic).append(" succeed");
				setCostomMsg(showMsg.toString());
			}
			
			@Override
			public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
				String msg =  "[Demo] Subscribe topic = "+ topic +" failed : " + exception.getMessage();
				setCostomMsg(msg);
				DemoUtil.showToast(msg, getApplicationContext());
				
				
			}
		});
		SharePrefsHelper.setString(getApplicationContext(), YunBaManager.LAST_SUB, topic);
	}

	private void publish() {
		final String topic = DemoUtil.Report_Topic;//pub_topic.getText().toString().trim();
		final String msg = pub_msg.getText().toString().trim();
		if (TextUtils.isEmpty(topic) || TextUtils.isEmpty(msg)) {
			Toast.makeText(MainActivity.this, "String should not be null", Toast.LENGTH_SHORT).show();
			return;
		}
		addTopic(topic);
		setCostomMsg("Publish msg = " + msg + " to topic = " + topic);
		YunBaManager.publish(getApplicationContext(), topic, msg, new IMqttActionListener() {
			public void onSuccess(IMqttToken asyncActionToken) {

				String msgLog = "Publish succeed : " + topic;
				StringBuilder showMsg = new StringBuilder();
				showMsg.append("[Demo] publish msg")
						.append(" = ").append(msg).append(" to ")
						.append(YunBaManager.MQTT_TOPIC).append(" = ").append(topic).append(" succeed");
				setCostomMsg(showMsg.toString());
				DemoUtil.showToast(msgLog, getApplicationContext());
			}
			
			@Override
			public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
				String msg = "[Demo] Publish topic = " + topic + " failed : " + exception.getMessage();
				setCostomMsg(msg);
				DemoUtil.showToast(msg, getApplicationContext());
				
			}
		});
		SharePrefsHelper.setString(getApplicationContext(), YunBaManager.LAST_PUB, topic);
	}

	private MessageReceiver mMessageReceiver;
	public void registerMessageReceiver() {
		mMessageReceiver = new MessageReceiver();
		IntentFilter filter = new IntentFilter();
		filter.addAction(YunBaManager.MESSAGE_RECEIVED_ACTION);
		filter.addCategory(getPackageName());
		registerReceiver(mMessageReceiver, filter);
		
		IntentFilter filterCon = new IntentFilter();
		filterCon.addAction(YunBaManager.MESSAGE_CONNECTED_ACTION);
		filterCon.addCategory(getPackageName());
		registerReceiver(mMessageReceiver, filterCon);
		
		IntentFilter filterDis = new IntentFilter();
		filterDis.addAction(YunBaManager.MESSAGE_DISCONNECTED_ACTION);
		filterDis.addCategory(getPackageName());
		registerReceiver(mMessageReceiver, filterDis);
		
		IntentFilter pres = new IntentFilter();
		pres.addAction(YunBaManager.PRESENCE_RECEIVED_ACTION);
		pres.addCategory(getPackageName());
		registerReceiver(mMessageReceiver, pres);

		IntentFilter lock = new IntentFilter();
		lock.addAction(LOCK_STATUS_ACTION);
		lock.addCategory(getPackageName());
		registerReceiver(mMessageReceiver, lock);

		IntentFilter unlock = new IntentFilter();
		unlock.addAction(UNLOCK_STATUS_ACTION);
		unlock.addCategory(getPackageName());
		registerReceiver(mMessageReceiver, unlock);
	}
	private void CleanAlarm(){
		if (mediaPlayer!=null){
			if (mediaPlayer.isPlaying()) {
				mediaPlayer.stop();
			}
			mediaPlayer.release();
			mediaPlayer=null;
		}
		/*CancleDTMF();*/
		ControlGpio.Closeaudio();
		// AlarmManager am = (AlarmManager) getSystemService(Activity.ALARM_SERVICE);
		//  Intent intent = new Intent(this,AlarmReceiver.class);
		// //  PendingIntent pi = PendingIntent.getBroadcast(this, AlarmPostion, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		//  am.cancel(pi);
	}
	private void CancleDTMF(){
		if (mToneGenerator!=null) {
			mToneGenerator.release();
			mToneGenerator = null;
		}
	}
	private void PlayToneDTMF(){
		mToneGenerator = new ToneGenerator(
				AudioManager.STREAM_DTMF, 99); // 设置声音的大小
		setVolumeControlStream(AudioManager.STREAM_DTMF);
		if (mToneGenerator!=null) {
			mToneGenerator.startTone(mToneGenerator.TONE_CDMA_HIGH_PBX_SLS,80);   //发出声音
		}
	}
	//获取系统默认铃声的Uri
	private Uri getSystemDefultRingtoneUri() {
		return RingtoneManager.getActualDefaultRingtoneUri(this,
				RingtoneManager.TYPE_ALARM);
	}
	private void PlayAlarmRing(){
		if (mediaPlayer==null) {
			mediaPlayer = new MediaPlayer();
			if (mediaPlayer == null) {
				return;
			}
			if (mediaPlayer.isPlaying()) {
				return;
			}
			try {
				mediaPlayer.setDataSource(this, getSystemDefultRingtoneUri());
				mediaPlayer.prepare();
				mediaPlayer.start();
			} catch (IOException e) {
			}
		}
		/*if (mToneGenerator==null) {
			PlayToneDTMF();
		}*/
		ControlGpio.Openaudio();
		new Handler().postDelayed(new Runnable(){
			public void run() {
				CleanAlarm();
			}
		}, 2500);
		/*try {
			Thread.sleep(5000);
			CleanAlarm();
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}*/
	}
	public void getservicedata(String data){
		Log.d("getservicedata",data);
		String cmd;
		try {
			JSONObject jsonObjs = new JSONObject(data);
			cmd=((JSONObject)jsonObjs).getString("cmd");
			if (cmd.equals("report")){
				GetJsonData();
				Log.d("cmd:",cmd);
			}else if (cmd.equals("unlock")){
				//DemoUtil.setlockstatus(false,getApplicationContext());
				PhoneVibrate();
				Log.d("cmd:",cmd);
			}else if (cmd.equals("buzzer")){
				PlayAlarmRing();
				Log.d("cmd:",cmd);
			}else if (cmd.equals("blink")){
				ControlLeds.ControlLeds();
				Log.d("cmd:",cmd);
			}
		}catch (JSONException ee){
			Log.d("getservicedata","JSONObject");
		}
	}

	private boolean isReceivePreUnlockCmd = false;

	/**
	 * 收到来自服务器新协议的消息
	 * @param data
     */
	public void onReceiveDataFromServerForNewProtocol(String data) {
		try {
			JSONObject msgJson = new JSONObject(data);
			int cmd = msgJson.getInt("a");
			switch (cmd) {
				case Constants.NewProtocolCmdAction.pre_unlock:
					isReceivePreUnlockCmd = true;
					break;
				case Constants.NewProtocolCmdAction.do_unlock:
					String sid = msgJson.getString("s");
					NewProtocolUtil.saveSessionId(getApplicationContext(), sid);
					if (isReceivePreUnlockCmd) {
						// 此处执行开锁操作
						PhoneVibrate();
					}
					break;
				case Constants.NewProtocolCmdAction.confirm_lock:
					// 确定上锁状态，此处应该检查锁的状态，此处以ControlGpio.getlockstatus()表示（但是不知道该行为是否会引起系统进行锁状态的广播）
					ControlGpio.getlockstatus();
					break;
				case Constants.NewProtocolCmdAction.set_alive_interval:
					// 取心跳间隔时间
					int aliveIntervalSecond = msgJson.getInt("i");
					SharePrefsHelper.setInt(getApplicationContext(), Constants.SP_PING_INTERVAL_KEY, aliveIntervalSecond);
					break;
				case Constants.NewProtocolCmdAction.enable_report_gps:
					// 获取是否进行GPS上报
					int enable = msgJson.getInt("e");
					SharePrefsHelper.setInt(getApplicationContext(), Constants.SP_ENABLE_GPS_KEY, enable);
					break;
				case Constants.NewProtocolCmdAction.gps_request:
					// 立即上报GPS信息
					doGPSResponse();
					break;
				case Constants.NewProtocolCmdAction.upgrade:
					// 执行固件升级的操作
					break;
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 向服务器上报GPS信息，上报参数中的g需要和新协议核对
	 */
	private void doGPSResponse() {
		if (locationgps==null) {
			locationgps = new Gps(this);
		}

		JSONObject gpsResponsePayload = new JSONObject();
		try {
			gpsResponsePayload.put("a", Constants.NewProtocolCmdAction.gps_response);
			gpsResponsePayload.put("s", 0);
			gpsResponsePayload.put("g", locationgps.getstringLocattion());
			YunBaManager.publish(getApplication(), Constants.gpsResponseTopic, gpsResponsePayload.toString(), new IMqttActionListener() {
				@Override
				public void onSuccess(IMqttToken iMqttToken) {

				}

				@Override
				public void onFailure(IMqttToken iMqttToken, Throwable throwable) {

				}
			});
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	//新的协议
	/*pre_unlock ： 1 ；
	do_unlock ： 2 ；
	lock_request ： 6 ；
	comfirm_lock ： 8 ；
	set_alive_interval ： 10 ；
	enable_report_gps ： 11 ；
	gps_request ： 12 ；
	gps_response ： 13 ；
	upgrade ： 14 ；*/
	public void getnewservicedata(String data){
		Log.d("getservicedata",data);
		data="{a:10,s:12}";
		String cmd;
		try {
			JSONObject jsonObjs = new JSONObject(data);
			cmd=((JSONObject)jsonObjs).getString("a");
			if (cmd.equals("1")){//pre_unlock ： 1
				Log.d("cmd:",cmd);
			}else if (cmd.equals("2")){//do_unlock ： 2
				Log.d("cmd:",cmd);
			}else if (cmd.equals("6")){//lock_request ： 6
				Log.d("cmd:",cmd);
			}else if (cmd.equals("8")){//comfirm_lock ： 8
				Log.d("cmd:",cmd);
			}else if (cmd.equals("10")){//set_alive_interval ： 10 ；
				Log.d("cmd:",cmd);
			}else if (cmd.equals("11")){//enable_report_gps ： 11 ；
				Log.d("cmd:",cmd);
			}else if (cmd.equals("12")){//gps_request ： 12 ；
				Log.d("cmd:",cmd);
			}else if (cmd.equals("13")){//gps_response ： 13 ；
				Log.d("cmd:",cmd);
			}else if (cmd.equals("14")){//upgrade ： 14
				Log.d("cmd:",cmd);
			}
		}catch (JSONException ee){
			Log.d("getnewservicedata","JSONObject");
		}
	}
	public class MessageReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
		    Log.i(TAG, "Action - " + intent.getAction());
			if (YunBaManager.MESSAGE_RECEIVED_ACTION.equals(intent.getAction())) {
				String status = "YunBa - Connected";
				setTitleOfApp(status);	
				String topic = intent.getStringExtra(YunBaManager.MQTT_TOPIC);
				String msg = intent.getStringExtra(YunBaManager.MQTT_MSG);
				StringBuilder showMsg = new StringBuilder();
				showMsg.append("[Message] ").append(YunBaManager.MQTT_TOPIC)
						.append(" = ").append(topic).append(" ,")
						.append(YunBaManager.MQTT_MSG).append(" = ").append(msg);
				setCostomMsg(showMsg.toString());

				//新协议收到消息的topic为 ,ybl/ + 单车编号，单车编号此处未知
				String newProtocolTopic = Constants.newProtocolMessageTopicPrefix + "";
				if (newProtocolTopic.equals(topic)) {
					// 按照新协议处理
					onReceiveDataFromServerForNewProtocol(msg);
					return;
				}
			///	getnewservicedata(msg);
				getservicedata(msg);
				if (topic.equals(DemoUtil.Report_Topic)){
					Log.d(TAG,"Report_Topic:"+showMsg.toString());
				}else {
					Log.d(TAG, "Receiver_Topic:"+showMsg.toString());
				}
			} else if(YunBaManager.MESSAGE_CONNECTED_ACTION.equals(intent.getAction())) {
				setCostomMsg("[YunBa] Connected");
				String status = "YunBa - Connected";
				setTitleOfApp(status);
				SharePrefsHelper.setString(getApplicationContext(), CONNECT_STATUS, status);
				DisplayImei();
				///Log.d("zhunengqin","NetworkConnectsuccessLeds");
				NetworkConnectsuccessLeds();
			} else if(YunBaManager.MESSAGE_DISCONNECTED_ACTION.equals(intent.getAction())) {
				setCostomMsg("[YunBa] DisConnected");
				String status = "YunBa - DisConnected";
				setTitleOfApp(status);
				SharePrefsHelper.setString(getApplicationContext(), CONNECT_STATUS, status);
				DisplayImei();
			} else if (YunBaManager.PRESENCE_RECEIVED_ACTION.equals(intent.getAction())) {
				String status = "YunBa - Connected";
				setTitleOfApp(status);
				String topic = intent.getStringExtra(YunBaManager.MQTT_TOPIC);
				String msg = intent.getStringExtra(YunBaManager.MQTT_MSG);
				StringBuilder showMsg = new StringBuilder();
				showMsg.append("[Message from prensence] ").append(YunBaManager.MQTT_TOPIC)
						.append(" = ").append(topic).append(" ,")
						.append(YunBaManager.MQTT_MSG).append(" = ").append(msg);
				setCostomMsg(showMsg.toString());
		    }else if(LOCK_STATUS_ACTION.equals(intent.getAction())){
				DemoUtil.setlockstatus(true,getApplicationContext());
				GetJsonData();
				PlayAlarmRing();
				Log.d("zhunengqin","locklock");
				//PhoneVibrate();

				//新协议上报关锁
				NewProtocolUtil.reportLockRequest(getApplicationContext());
				isReceivePreUnlockCmd = false;
			}else if(UNLOCK_STATUS_ACTION.equals(intent.getAction())){
				DemoUtil.setlockstatus(false,getApplicationContext());
				GetJsonData();
				stopPhoneVibrate();
				PlayAlarmRing();
				//PhoneVibrate();
			}
		}
	}

	private void setCostomMsg(final String msg){
		 YunBaTabActivity.setCostomMsg(this, msg);
	}
	
	private List<String> topics = new ArrayList<String>();


	private void addTopic(String topic)  {
		if(DemoUtil.isEmpty(topic)) return;
		for (int i = 0; i < topics.size(); i++) {
			if (topic.equals(topics.get(i))) return;
		}
		topics.add(topic);
		Log.i(TAG, "Topic size = " + topics.size());
	}
	
	private void showTopic(final EditText text){
		 final String[] topicArr = topics.toArray(new String[0]);
			Log.i("Topic", "topicArr size = " + topicArr.length);
		 new AlertDialog.Builder(MainActivity.this).setTitle("Select a topic")
		 .setItems(topicArr,new DialogInterface.OnClickListener(){  
		      public void onClick(DialogInterface dialog, int which){  
		    	  text.setText(topicArr[which]);
		       dialog.dismiss();  
		      }  
		   }).show();
	} 
	
	private void  setTitleOfApp(final String status) {
		
		Activity parent = this.getParent();
		if(!DemoUtil.isEmpty(status) && null != parent) {
			this.getParent().setTitle(status);
		}
	}
}
