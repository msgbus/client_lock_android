package com.pashine.pashineyunba.telephone;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


public class PhoneCall {
    public static final String PHONE_NUMBER = "phoneNumber";
    public static final String PHONE_CALL_TYPE = "type";

    public enum CallType {
        Incoming, Outgoing, Dailer, Accept,
    }

    public final String number;
    public final CallType type;

    public PhoneCall(String number, CallType type) {
        this.number = number;
        this.type = type;
    }

    public PhoneCall(Intent intent) {
        this.number = intent.getStringExtra(PHONE_NUMBER);
        this.type = (CallType) intent.getSerializableExtra(PHONE_CALL_TYPE);
    }

    public void save(Context context){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit = sp.edit();
        edit.putString(PHONE_NUMBER, this.number);
        edit.putString(PHONE_CALL_TYPE, this.type.name());
        edit.commit();
    }

    public static PhoneCall load(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        if(sp.contains(PHONE_NUMBER)) {

            final String phoneNumber = sp.getString(PHONE_NUMBER, "none");
            final CallType callType = CallType.valueOf(sp.getString(PHONE_CALL_TYPE, CallType.Incoming.name()));

            return new PhoneCall(phoneNumber, callType);
        }

        return null;

    }
}
