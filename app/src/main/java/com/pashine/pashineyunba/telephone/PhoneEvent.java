package com.pashine.pashineyunba.telephone;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


public enum PhoneEvent {
    No_Call, Ringing_Incoming, Ringing_Outgoing, Off_Hook, Idle;
    public static final String PHONE_EVENT = "PhoneEvent";

    public void save(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit = sp.edit();
        edit.putString(PHONE_EVENT, name());
        edit.commit();
    }

    public static PhoneEvent load(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);

        if(sp.contains(PHONE_EVENT)) {
            return valueOf(sp.getString(PHONE_EVENT, No_Call.name()));
        }

        return No_Call;
    }
}
