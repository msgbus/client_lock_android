package com.pashine.pashineyunba.telephone;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;

public class PhoneCallReceiver extends BroadcastReceiver {
    public static final String TAG = PhoneCallReceiver.class.getName();

    public void onReceive(Context context, Intent intent) {
        if (isOutgoing(intent)) {

            String phoneNumber = getResultData();
            if (phoneNumber == null) {
                // No reformatted number, use the original
                phoneNumber = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
            }

            //startService(context, phoneNumber, PhoneCall.CallType.Outgoing, PhoneEvent.Ringing_Outgoing);
            Log.d(TAG, "phone state Outgoing call : " + phoneNumber);
            //startActivity(context, phoneNumber, PhoneCall.CallType.Outgoing);
          //  MedicalWatchCallActivity.newInstance(context, phoneNumber, PhoneCall.CallType.Outgoing);


            // Let things continue on so that we can let other dialers have their chance
            setResultData(phoneNumber);

        } else if (isPhoneState(intent)) {

            Bundle extras = intent.getExtras();
            if (extras != null) {
                String state = extras.getString(TelephonyManager.EXTRA_STATE);

                if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
                    String phoneNumber = extras
                            .getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
                    //startService(context, phoneNumber, PhoneCall.CallType.Incoming, PhoneEvent.Ringing_Incoming);
                    Log.d(TAG, "phone state Incoming call : " + phoneNumber);
                  //  MedicalWatchCallActivity.newInstance(context, phoneNumber, PhoneCall.CallType.Incoming);

                } else if (state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
                    Log.d(TAG, "phone state off hook");
                  //  MedicalWatchCallActivity.setOffHook();
                } else if (state.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
                    Log.d(TAG, "phone state idle");
                 //   MedicalWatchCallActivity.destroyInstance();
                }

                //AlarmHandleThread.updateCallState(state); //更新报警线程打电话状态
            }
        } else if (isCallActive(intent)) {
            Log.d(TAG, "phone state call active");
           // MedicalWatchCallActivity.setCallActive();
        }
    }

    private boolean isPhoneState(Intent intent) {
        return intent.getAction().equals("android.intent.action.PHONE_STATE");
    }

    private boolean isOutgoing(Intent intent) {
        return intent.getAction().equals("android.intent.action.NEW_OUTGOING_CALL");
    }

    private boolean isCallActive(Intent intent) {
        return intent.getAction().equals("android.intent.action.CallState.ACTIVE");
    }
}
