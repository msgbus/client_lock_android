package com.pashine.pashineyunba.telephone;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.IBinder;
import android.telephony.NeighboringCellInfo;
import android.telephony.TelephonyManager;
import com.android.internal.telephony.ITelephony;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

public class telephoneCall {
	public static final String SERVICEMANAGER = "android.os.ServiceManager";

	public static void call(String number, Context mContext) {
//		Class<TelephonyManager> c = TelephonyManager.class;
//		Method getITelephonyMethod = null;
//		try {
//			getITelephonyMethod = c.getDeclaredMethod("getITelephony",
//					(Class[]) null);
//			getITelephonyMethod.setAccessible(true);
//		} catch (SecurityException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (NoSuchMethodException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		try {
//			TelephonyManager tManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
//			Object iTelephony;
//			iTelephony = (Object) getITelephonyMethod.invoke(tManager, (Object[]) null);
//			//   String telename=iTelephony.getClass().toString();
//			//   Log.i(TAG, "tele name"+telename);
//			// Method dial = iTelephony.getClass().getDeclaredMethod("call", String.class);
//			Method dial = iTelephony.getClass().getDeclaredMethod("call", String.class, String.class);
//			// dial.invoke(iTelephony, number);
//			String mypackage = "com.pashine.watch";
//			dial.invoke(iTelephony, mypackage, number);
//		} catch (IllegalArgumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IllegalAccessException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (SecurityException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (NoSuchMethodException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (InvocationTargetException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		dial(number, mContext);
	}

//	public static void endCall(Context mContext) {
//		Class<TelephonyManager> c = TelephonyManager.class;
//		Method getITelephonyMethod = null;
//		try {
//			getITelephonyMethod = c.getDeclaredMethod("getITelephony",
//					(Class[]) null);
//			getITelephonyMethod.setAccessible(true);
//		} catch (SecurityException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (NoSuchMethodException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		try {
//			TelephonyManager tManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
//			if (tManager.getCallState() != TelephonyManager.CALL_STATE_IDLE) {
//				Object iTelephony;
//				iTelephony = (Object) getITelephonyMethod.invoke(tManager, (Object[]) null);
//				Method end = iTelephony.getClass().getDeclaredMethod("endCall", (Class[]) null);
//				end.invoke(iTelephony, (Object[]) null);
//			}
//		} catch (IllegalArgumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IllegalAccessException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (SecurityException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (NoSuchMethodException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (InvocationTargetException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}

	public static void dial(String number, Context mContext) {
		/*
		Class<TelephonyManager> c = TelephonyManager.class;
		Method getITelephonyMethod = null;
		try {
			getITelephonyMethod = c.getDeclaredMethod("getITelephony",
					(Class[]) null);
			getITelephonyMethod.setAccessible(true);
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			TelephonyManager tManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
			Object iTelephony;
			iTelephony = (Object) getITelephonyMethod.invoke(tManager,(Object[]) null);
			Method dial = iTelephony.getClass().getDeclaredMethod("dial", String.class);
			dial.invoke(iTelephony, number);
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/

		TelephonyManager tManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
		if (tManager.getCallState() != TelephonyManager.CALL_STATE_IDLE) {
			return;
		}

		String callNumber = "tel:" + number;
		Intent intent = new Intent("android.intent.action.CALL_PRIVILEGED", Uri.parse(callNumber));
		//Intent intent = new Intent("android.intent.action.CALL_EMERGENCY", Uri.parse(callNumber));
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		mContext.startActivity(intent);
//		call(number, mContext);
//		call(number);
	}

	public static void answerRingingCall(Context mContext) {
		Class<TelephonyManager> c = TelephonyManager.class;
		Method getITelephonyMethod = null;
		try {
			getITelephonyMethod = c.getDeclaredMethod("getITelephony",
					(Class[]) null);
			getITelephonyMethod.setAccessible(true);
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			TelephonyManager tManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
			Object iTelephony;
			iTelephony = (Object) getITelephonyMethod.invoke(tManager, (Object[]) null);
			Method answer = iTelephony.getClass().getDeclaredMethod("answerRingingCall", (Class[]) null);
			answer.invoke(iTelephony, (Object[]) null);
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static void call(String number) {
		try {
			Method method = Class.forName(SERVICEMANAGER).getMethod("getService", String.class);
			IBinder binder = (IBinder) method.invoke(null, new Object[]{Context.TELEPHONY_SERVICE});
			ITelephony telephony = ITelephony.Stub.asInterface(binder);
			telephony.call("com.pashine.watch", number);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void endCall() {

		try {
			Method method = Class.forName(SERVICEMANAGER).getMethod("getService", String.class);
			IBinder binder = (IBinder) method.invoke(null, new Object[]{Context.TELEPHONY_SERVICE});
			ITelephony telephony = ITelephony.Stub.asInterface(binder);
			telephony.endCall();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void answerRingingCall() {

		try {
			Method method = Class.forName(SERVICEMANAGER).getMethod("getService", String.class);
			IBinder binder = (IBinder) method.invoke(null, new Object[]{Context.TELEPHONY_SERVICE});
			ITelephony telephony = ITelephony.Stub.asInterface(binder);
			telephony.answerRingingCall();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static List<NeighboringCellInfo> getNeighboringCellInfo() {
		try {
			Method method = Class.forName(SERVICEMANAGER).getMethod("getService", String.class);
			IBinder binder = (IBinder) method.invoke(null, new Object[]{Context.TELEPHONY_SERVICE});
			ITelephony telephony = ITelephony.Stub.asInterface(binder);
			return telephony.getNeighboringCellInfo("com.pashine.pashineyunba");

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public static void sendDtmf(char keycode) {
		try {

			Class cls_phoneFactory = Class
					.forName("com.android.internal.telephony.PhoneFactory");
			Method method_getDefaultPhone = cls_phoneFactory.getDeclaredMethod(
					"getDefaultPhone", (Class[])null);
			method_getDefaultPhone.setAccessible(true);
			Object obj_phone = method_getDefaultPhone.invoke(null);
			Method method_sendDTMF = obj_phone.getClass().getDeclaredMethod(
					"sendDtmf", char.class);
			method_sendDTMF.invoke(obj_phone, keycode);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String getDeviceId(Context context) {
		final String defaultIMEI = "000000000000000";
		TelephonyManager tManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		return tManager.getDeviceId() == null ? defaultIMEI : tManager.getDeviceId();
	}

}
