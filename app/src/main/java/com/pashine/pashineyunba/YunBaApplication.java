package com.pashine.pashineyunba;

import android.app.Application;
import android.util.Log;

import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;

import io.yunba.android.manager.YunBaManager;

public class YunBaApplication extends Application {
	private final static String TAG = "YunBaApplication";
	@Override
	public void onCreate() {
		super.onCreate();
		Thread thread=new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				while (true) {
					String imei=DemoUtil.Demogetimei(getApplicationContext());
					if ( imei!= null && imei.substring(imei.length()-10).equals(DemoUtil.Report_Topic)) {
						startBlackService();
						return;
					}
				}
			}
		});
		thread.start();
		initConnectStatus();
		///startBlackService();

	}

	private void initConnectStatus() {
		//set MainActivity title status
		SharePrefsHelper.setString(getApplicationContext(), MainActivity.CONNECT_STATUS, "");
	}
	private void startBlackService() {
		YunBaManager.setThirdPartyEnable(getApplicationContext(), true);
		YunBaManager.start(getApplicationContext());
		IMqttActionListener listener = new IMqttActionListener() {
			
			@Override
			public void onSuccess(IMqttToken asyncActionToken) {
				String topic = DemoUtil.join(asyncActionToken.getTopics(), ",");
				Log.d(TAG, "Subscribe succeed : " + topic);
//				DemoUtil.showToast( "Subscribe succeed : " + topic, getApplicationContext());
				StringBuilder showMsg = new StringBuilder();
				showMsg.append("subscribe succ：").append(YunBaManager.MQTT_TOPIC)
						.append(" = ").append(topic);
			}
			
			@Override
			public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
				String msg =  "Subscribe failed : " + exception.getMessage();
				Log.d(TAG, msg);
//				DemoUtil.showToast(msg, getApplicationContext());
//				
				
			}
		};
		//for test
		YunBaManager.subscribe(getApplicationContext(), new String[]{DemoUtil.Report_Topic}, listener);
		YunBaManager.setAlias(getApplicationContext(), DemoUtil.Receiver_Alias, new IMqttActionListener() {

			@Override
			public void onSuccess(IMqttToken arg) {
				StringBuilder showMsg = new StringBuilder();
				//DisplayImei(DemoUtil.Receiver_Alias);
				Log.d(TAG, "Receiver_Alias succeed : " + DemoUtil.Receiver_Alias);
				showMsg.append("[Demo] setAlias alias ")
						.append(" = ").append(DemoUtil.Receiver_Alias).append(" succeed");
				//NetworkConnectsuccessLeds();
			}

			@Override
			public void onFailure(IMqttToken arg0, Throwable arg) {
				StringBuilder showMsg = new StringBuilder();
				Log.d(TAG, "Receiver_Alias failed : " + DemoUtil.Receiver_Alias);
				showMsg.append("[Demo] setAlias alias ")
						.append(" = ").append(DemoUtil.Receiver_Alias).append(" failed");
			}
		});
	}


	@Override
	public void onTerminate() {
		super.onTerminate();
	}

	

}
