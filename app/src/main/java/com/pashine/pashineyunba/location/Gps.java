package com.pashine.pashineyunba.location;

import android.content.Context;
import android.location.Criteria;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;

public class Gps{
    private Location location = null;
    private LocationManager locationManager = null;
    private Context context = null;
    private String gpsstring ="";
    private OnLocationChangedListener onLocationChangedListener = null;
    /**
     * 初始化
     *
     * @param ctx
     */
    public Gps(Context ctx) {
        context=ctx;
        ControlGps(true);
        locationManager=(LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
        try {
            location = locationManager.getLastKnownLocation(getProvider());
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2000, 10, locationListener);
            locationManager.addNmeaListener(new GpsStatus.NmeaListener() {

                @Override
                public void onNmeaReceived(long timestamp, String nmea) {
                  ///  tvNmea.invalidate();
                    //此处以GPGGA为例
                    //$GPGGA,232427.000,3751.1956,N,11231.1494,E,1,6,1.20,824.4,M,-23.0,M,,*7E
                    if (nmea.contains("GPGGA")) {
                        gpsstring=nmea;
                        Log.d("onNmeaReceived",nmea);
                    }
                }
            });
        }catch (SecurityException ee){
            Log.d("GPS","SecurityException");
        }

    }
    public void ControlGps(boolean enable){
        //Settings.Secure.setLocationProviderEnabled(mcontext.getContentResolver(), LocationManager.GPS_PROVIDER, enable );
        if ((Settings.Secure.isLocationProviderEnabled(context.getContentResolver(), LocationManager.GPS_PROVIDER )) && enable){
            return;
        }else if ((!Settings.Secure.isLocationProviderEnabled(context.getContentResolver(), LocationManager.GPS_PROVIDER )) && !enable){
            return;
        }
        Settings.Secure.setLocationProviderEnabled(
                context.getContentResolver(), LocationManager.GPS_PROVIDER,
                enable);
    }
    // 获取Location Provider
    private String getProvider() {
        // 构建位置查询条件
        Criteria criteria = new Criteria();
        // 查询精度：高
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        // 是否查询海拨：否
        criteria.setAltitudeRequired(false);
        // 是否查询方位角 : 否
        criteria.setBearingRequired(false);
        // 是否允许付费：是
        criteria.setCostAllowed(true);
        // 电量要求：低
        criteria.setPowerRequirement(Criteria.POWER_LOW);
        // 返回最合适的符合条件的provider，第2个参数为true说明 , 如果只有一个provider是有效的,则返回当前provider
        return locationManager.getBestProvider(criteria, true);
    }
    private LocationListener locationListener = new LocationListener() {
        // 位置发生改变后调用
        public void onLocationChanged(Location l) {
            if(l!=null){
                location=l;
                ///getstringLocattion();///////////////////////////test
                if(onLocationChangedListener != null) {
                    onLocationChangedListener.onLocationChanged(l);
                }
            }
        }
        // provider 被用户关闭后调用
        public void onProviderDisabled(String provider) {
            location=null;
        }
        // provider 被用户开启后调用
        public void onProviderEnabled(String provider) {
            try {
                Location l = locationManager.getLastKnownLocation(provider);
               /// getstringLocattion();////////////////test
                if(l!=null){
                    location=l;
                    if(onLocationChangedListener != null) {
                        onLocationChangedListener.onLocationChanged(l);
                    }
                }
            }catch (SecurityException ee){
                    Log.d("GPS","SecurityException");
                }
        }
        // provider 状态变化时调用
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    };
    public Location getLocation(){
        return location;
    }

    public String getstringLocattion(){
        return gpsstring;
    }
    public String setstringLocattion(String str){
        return gpsstring=str;
    }
    public void requestUpdates() {
        try {
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2000, 10, locationListener);
        }catch (SecurityException ee){
            Log.d("GPS","SecurityException");
        }
    }

    public void removeUpdates() {
        try {
        locationManager.removeUpdates(locationListener);
        }catch (SecurityException ee){
            Log.d("GPS","SecurityException");
        }
    }
    public void closeLocation(){
        if(locationManager!=null){
            if(locationListener!=null){
                try {
                locationManager.removeUpdates(locationListener);
                locationListener=null;
                }catch (SecurityException ee){
                    Log.d("GPS","SecurityException");
                }
            }
            locationManager=null;
        }
    }


    public void setOnClickListener(OnLocationChangedListener onLocationChangedListener) {
        this.onLocationChangedListener = onLocationChangedListener;
    }
    public interface OnLocationChangedListener {
        void onLocationChanged(Location l);
    }
}
