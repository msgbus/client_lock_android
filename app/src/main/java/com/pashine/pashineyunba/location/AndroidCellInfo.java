package com.pashine.pashineyunba.location;

import android.content.Context;
import android.telephony.NeighboringCellInfo;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by saxer on 12/21/15.
 */
public class AndroidCellInfo {
    private static final String TAG = AndroidCellInfo.class.getSimpleName();

    public static class CellIDInfo {

        public int cellId;
        public String mobileCountryCode;
        public String mobileNetworkCode;
        public int locationAreaCode;
        public int systemId;
        public int netWorkId;
        public int signal;
        public String radioType;

        public CellIDInfo() {
        }
    }

    public static ArrayList<CellIDInfo> getCellInfoArray(Context context) {
        TelephonyManager manager = (TelephonyManager) context
                .getSystemService(Context.TELEPHONY_SERVICE);

        ArrayList<CellIDInfo> cellInfos = new ArrayList<>();
        CellIDInfo currentCell = new CellIDInfo();

        try {
            String radioType;

            if (manager.getCellLocation() instanceof GsmCellLocation) {
                GsmCellLocation location = (GsmCellLocation) manager.getCellLocation();
                currentCell.locationAreaCode = location.getLac();
                currentCell.cellId = location.getCid();
                radioType = "gsm";
            } else if (manager.getCellLocation() instanceof CdmaCellLocation) {
                CdmaCellLocation location = (CdmaCellLocation) manager.getCellLocation();
                currentCell.cellId = location.getBaseStationId();
                currentCell.systemId = location.getSystemId();
                currentCell.netWorkId = location.getNetworkId();
                radioType = "cdma";
            } else {
                return cellInfos;
            }

            String mcc = (manager.getSimOperator() != null && manager
                    .getSimOperator().length() >= 3) ? manager
                    .getSimOperator().substring(0, 3) : "";
            String mnc = (manager.getSimOperator() != null && manager
                    .getSimOperator().length() >= 5) ? manager
                    .getSimOperator().substring(3, 5) : "";

            currentCell.mobileCountryCode = mcc;
            currentCell.mobileNetworkCode = mnc;
           // currentCell.signal = MedicalWatchStatusBarFragment.getNetworkSignal();
            currentCell.radioType = radioType;

            cellInfos.add(currentCell);

            // 获得邻近基站信息
            List<NeighboringCellInfo> list = manager
                    .getNeighboringCellInfo();

            int size = list != null ? list.size() : 0;
            for (int i = 0; i < size; i++) {

                CellIDInfo info = new CellIDInfo();
                info.cellId = list.get(i).getCid();
                info.mobileCountryCode = mcc;
                info.mobileNetworkCode = mnc;
                info.locationAreaCode = currentCell.locationAreaCode;
                info.signal = list.get(i).getRssi();

                cellInfos.add(info);
            }

        } catch (Exception e) {
            Log.e(TAG, "Get cell id info fail " + e.getMessage());
        }
        return cellInfos;
    }
}
