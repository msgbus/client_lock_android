package com.pashine.pashineyunba.utils;


import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.pashine.pashineyunba.Constants;
import com.pashine.pashineyunba.DemoUtil;
import com.pashine.pashineyunba.SharePrefsHelper;
import com.pashine.pashineyunba.location.Gps;

import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.json.JSONArray;
import org.json.JSONObject;

import io.yunba.android.core.YunBaReceiver;
import io.yunba.android.manager.YunBaManager;

import static android.content.Context.ALARM_SERVICE;

public class PingManager {
    /**
     * 开始进行心跳
     */
    public static void schedulePing(Context context) {
        try {
            Intent intent = new Intent(context, YunBaReceiver.class);
            intent.setAction(Constants.ACTION_PING);
            PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            // Schedule the alarm!
            AlarmManager am = (AlarmManager) context.getSystemService(ALARM_SERVICE);
            // 缺省心跳值置为 200s
            int aliveInterval = SharePrefsHelper.getInt(context, Constants.SP_PING_INTERVAL_KEY, Constants.DEFAULT_PING_INTERVAL);
            if(null != am) {
                am.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 1000 * aliveInterval, sender);
            }
        } catch (Throwable e) {
        }
    }

    /**
     * 上报一次心跳
     */
    public static void reportHeartbeat(Context context) {
        CommonUtil.getWakeLock(context);
        JSONObject reportJson = new JSONObject();
        int enableGps = SharePrefsHelper.getInt(context, Constants.SP_ENABLE_GPS_KEY, 0);
        try {
            // 在开启GPS模块才上报该信息
            if (enableGps == Constants.ENABLE_GPS) {
                Gps gps = new Gps(context);
                reportJson.put("g", gps.getstringLocattion());
            }
            reportJson.put("b", DemoUtil.batterypercent);
            // 需要按照协议完善基站信息
            reportJson.put("c", new JSONArray());
            // 按照协议置为单车编号
            reportJson.put("i", "");
            reportJson.put("t", 0);
            boolean lockStatus = DemoUtil.getlockstatus(context);
            if (lockStatus) {
                reportJson.put("s", 2);
            } else {
                // 此处应该根据锁的状态置为相应的状态值 0 表示闲置，1 表示待机
                reportJson.put("s", 1);
            }

            YunBaManager.publish(context, Constants.lockpingTopic, reportJson.toString(), new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken iMqttToken) {
                    CommonUtil.stopWakeLock();
                }

                @Override
                public void onFailure(IMqttToken iMqttToken, Throwable throwable) {
                    CommonUtil.stopWakeLock();
                }
            });
        } catch (Exception e) {
            CommonUtil.stopWakeLock();
            e.printStackTrace();
        }

    }
}
