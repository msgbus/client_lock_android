package com.pashine.pashineyunba.utils;


import android.content.Context;
import android.os.PowerManager;

public class CommonUtil {

    public static void getWakeLock(Context context){
        try {
            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            PowerManager.WakeLock wl = pm.newWakeLock(
                    PowerManager.PARTIAL_WAKE_LOCK,
                    "MQTTPUSH");
            WakelockManager.getInstance().setWakelock(wl);
            if (!WakelockManager.getInstance().getWakelock().isHeld()) {
                WakelockManager.getInstance().getWakelock().acquire();
            }
        } catch (Exception e) {
        }
    }

    public static void stopWakeLock(){
        try {
            // Release wake lock
            PowerManager.WakeLock wl = WakelockManager.getInstance().getWakelock();

            if (wl != null) {
                if (wl.isHeld()) {
                    wl.release();
                }
            }
        } catch (Exception e) {
        }
    }
}
