package com.pashine.pashineyunba.utils;


import android.content.Context;

import com.pashine.pashineyunba.Constants;
import com.pashine.pashineyunba.SharePrefsHelper;

import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.json.JSONException;
import org.json.JSONObject;

import io.yunba.android.manager.YunBaManager;

public class NewProtocolUtil {

    private static IMqttActionListener defaultMqttActionListener = new IMqttActionListener() {
        @Override
        public void onSuccess(IMqttToken iMqttToken) {

        }

        @Override
        public void onFailure(IMqttToken iMqttToken, Throwable throwable) {

        }
    };


    public static void reportLockRequest(final Context context) {
        JSONObject lockRequestJson = new JSONObject();
        try {
            lockRequestJson.put("a", Constants.NewProtocolCmdAction.lock_request);
            lockRequestJson.put("s", SharePrefsHelper.getString(context, Constants.SP_SESSION_ID_KEY, ""));

            YunBaManager.publish(context, Constants.lockRequestTopic, lockRequestJson.toString(), new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken iMqttToken) {
                    clearSessionCache(context);
                }

                @Override
                public void onFailure(IMqttToken iMqttToken, Throwable throwable) {
                    // 由于某些情况（网络原因）导致上传失败，必须重现上报。这里简单处理，但是应该具有一定的策略性
                    reportLockRequest(context);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * 清理骑行session_id
     * @param context
     */
    public static void clearSessionCache(Context context) {
        SharePrefsHelper.setString(context, Constants.SP_SESSION_ID_KEY, "");
    }

    /**
     * 保存骑行session id
     * @param context
     * @param sid
     */
    public static void saveSessionId(Context context, String sid) {
        SharePrefsHelper.setString(context, Constants.SP_SESSION_ID_KEY, sid);
    }
}
