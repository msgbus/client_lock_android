package com.pashine.pashineyunba.utils;


import com.pashine.pashineyunba.DemoUtil;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by root on 16-3-7.
 */
public class ControlLeds {
    private static final String TAG = "ControlLeds";
    public static OutputStream controlstream = null;
    public static OutputStream Redcontrolstream = null;
    public static OutputStream Bluecontrolstream = null;
    public static  void ControlGpio() {
        CloseControl();
        File file;
        file = new File("/sys/devices/platform/leds-mt65xx/leds/green/brightness");
        if (file.exists()) {
            try {
                controlstream = new BufferedOutputStream(new FileOutputStream(file));
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public static void CloseControl() {
        if (controlstream != null) {
            try {
                controlstream.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            controlstream = null;
        }
    }
    public static void LedPowerControl(byte[] data) {
		/*
		 *
		 * try { File file=new
		 * File("/sys/bus/platform/drivers/irled/irled_val");
		 *
		 * OutputStream out = null; if(file.exists()) { //
		 * Log.i(TAG,"write led power control"); out = new
		 * BufferedOutputStream(new FileOutputStream(file));
		 *
		 * out.write(data); out.flush(); out.close(); }
		 *
		 * } catch(Exception e) { e.printStackTrace(); }
		 */
        ControlGpio();
        try {
            if (controlstream != null) {
                controlstream.write(data);
                controlstream.flush();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    ///red
    public static  void RedControlGpio() {
        RedCloseControl();
        File file;
        file = new File("/sys/devices/platform/leds-mt65xx/leds/red/brightness");
        if (file.exists()) {
            try {
                Redcontrolstream = new BufferedOutputStream(new FileOutputStream(file));
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public static void RedCloseControl() {
        if (Redcontrolstream != null) {
            try {
                Redcontrolstream.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            Redcontrolstream = null;
        }
    }
    public static void RedLedPowerControl(byte[] data) {
		/*
		 *
		 * try { File file=new
		 * File("/sys/bus/platform/drivers/irled/irled_val");
		 *
		 * OutputStream out = null; if(file.exists()) { //
		 * Log.i(TAG,"write led power control"); out = new
		 * BufferedOutputStream(new FileOutputStream(file));
		 *
		 * out.write(data); out.flush(); out.close(); }
		 *
		 * } catch(Exception e) { e.printStackTrace(); }
		 */
        RedControlGpio();
        try {
            if (Redcontrolstream != null) {
                Redcontrolstream.write(data);
                Redcontrolstream.flush();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//blue
    public static  void BlueControlGpio() {
        BlueCloseControl();
        File file;
        file = new File("/sys/devices/platform/leds-mt65xx/leds/blue/brightness");
        if (file.exists()) {
            try {
                Bluecontrolstream = new BufferedOutputStream(new FileOutputStream(file));
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public static void BlueCloseControl() {
        if (Bluecontrolstream != null) {
            try {
                Bluecontrolstream.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            Bluecontrolstream = null;
        }
    }
    public static void BlueLedPowerControl(byte[] data) {
		/*
		 *
		 * try { File file=new
		 * File("/sys/bus/platform/drivers/irled/irled_val");
		 *
		 * OutputStream out = null; if(file.exists()) { //
		 * Log.i(TAG,"write led power control"); out = new
		 * BufferedOutputStream(new FileOutputStream(file));
		 *
		 * out.write(data); out.flush(); out.close(); }
		 *
		 * } catch(Exception e) { e.printStackTrace(); }
		 */
        BlueControlGpio();
        try {
            if (Bluecontrolstream != null) {
                Bluecontrolstream.write(data);
                Bluecontrolstream.flush();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private static void delay(int ms){
        try {
            Thread.currentThread();
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public static void ControlLeds(){
        byte[] power_on_data = new byte[] {'1',0, 0 };//12set gpio0=1 56
        for (int i=0;i<5;i++) {
            power_on_data = new byte[]{'1', 0, 0};
            LedPowerControl(power_on_data);
            delay(1000);
            power_on_data = new byte[]{'0', 0, 0};//12set gpio0=1 56
            LedPowerControl(power_on_data);
            delay(1000);
        }
        if (DemoUtil.BatteryACORUSBPLUG){
            power_on_data = new byte[]{'1', 0, 0};
            LedPowerControl(power_on_data);
        }
    }
    public static void TurnOnControlLeds(){
        byte[] power_on_data = new byte[] {'1',0, 0 };//12set gpio0=1 56
        power_on_data = new byte[]{'1', 0, 0};
        RedLedPowerControl(power_on_data);
        delay(1000);
        power_on_data = new byte[]{'0', 0, 0};//12set gpio0=1 56
        RedLedPowerControl(power_on_data);
        delay(1000);
        power_on_data = new byte[]{'1', 0, 0};
        BlueLedPowerControl(power_on_data);
        delay(1000);
        power_on_data = new byte[]{'0', 0, 0};//12set gpio0=1 56
        BlueLedPowerControl(power_on_data);
        delay(1000);
        power_on_data = new byte[]{'1', 0, 0};
        LedPowerControl(power_on_data);
        delay(1000);
        power_on_data = new byte[]{'0', 0, 0};//12set gpio0=1 56
        LedPowerControl(power_on_data);
        delay(1000);
        if (DemoUtil.BatteryACORUSBPLUG){
            power_on_data = new byte[]{'1', 0, 0};
            LedPowerControl(power_on_data);
        }
    }
    public static void NetworkConnectsuccessLeds(){
        byte[] power_on_data = new byte[] {'1',0, 0 };//12set gpio0=1 56
        for (int i=0;i<2;i++) {
            power_on_data = new byte[]{'1', 0, 0};
            LedPowerControl(power_on_data);
            delay(1000);
            power_on_data = new byte[]{'0', 0, 0};//12set gpio0=1 56
            LedPowerControl(power_on_data);
            delay(1000);
        }
        if (DemoUtil.BatteryACORUSBPLUG){
            power_on_data = new byte[]{'1', 0, 0};
            LedPowerControl(power_on_data);
        }
    }

}
