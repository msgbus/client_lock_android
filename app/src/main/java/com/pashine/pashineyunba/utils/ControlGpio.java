package com.pashine.pashineyunba.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Build;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by root on 16-3-7.
 */
public class ControlGpio {
    private static final String TAG = "ControlGpio";
    public static OutputStream controlstream = null;
    public static  void audio_ControlGpio() {
        audio_CloseControl();
        File file = new File("/sys/bus/platform/devices/extpwr.0/driver/extpwr_val");
        if (file.exists()) {
            try {
                controlstream = new BufferedOutputStream(new FileOutputStream(file));
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public static void audio_CloseControl() {
        if (controlstream != null) {
            try {
                controlstream.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            controlstream = null;
        }
    }
/*
    byte[] power_on_data = new byte[] { '0',0, 0 };//////set gpio0=0
	ControlGpio(power_on_data);
	byte[] power_on_data = new byte[] { '1',0, 0 };//////set gpio0=1
	ControlGpio(power_on_data);
	byte[] power_on_data = new byte[] { '2',0, 0 };//////set gpio1=0
	ControlGpio(power_on_data);
	byte[] power_on_data = new byte[] { '3',0, 0 };//////set gpi1=1
	ControlGpio(power_on_data);
	byte[] power_on_data = new byte[] { '4',0, 0 };//////set gpi2=0
	ControlGpio(power_on_data);
	byte[] power_on_data = new byte[] { '5',0, 0 };//////set gpio2=1
	ControlGpio(power_on_data);
		byte[] power_on_data = new byte[] { '6',0, 0 };//////set VMCH:0V
	ControlGpio(power_on_data);
	byte[] power_on_data = new byte[] { '7',0, 0 };//////set VMCH:3.3V
	ControlGpio(power_on_data);
 */
    public static void audio_LedPowerControl(byte[] data) {
		/*
		 *
		 * try { File file=new
		 * File("/sys/bus/platform/drivers/irled/irled_val");
		 *
		 * OutputStream out = null; if(file.exists()) { //
		 * Log.i(TAG,"write led power control"); out = new
		 * BufferedOutputStream(new FileOutputStream(file));
		 *
		 * out.write(data); out.flush(); out.close(); }
		 *
		 * } catch(Exception e) { e.printStackTrace(); }
		 */
        audio_ControlGpio();
        try {
            if (controlstream != null) {
                controlstream.write(data);
                controlstream.flush();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private static void delay(int ms){
        try {
            Thread.currentThread();
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public static void Openaudio(){
        byte[] power_on_data = new byte[] { '1',0, 0 };
        for (int i=0;i<1;i++) {
            power_on_data = new byte[]{'1', 0, 0};
            audio_LedPowerControl(power_on_data);
            delay(50);
            power_on_data = new byte[]{'0', 0, 0};
            audio_LedPowerControl(power_on_data);
            delay(100);
        }
    }
    public static void Closeaudio(){
        byte[] power_on_data = new byte[] { '0',0, 0 };
        audio_LedPowerControl(power_on_data);
    }
    public static void getlockstatus(){
        byte[] power_on_data = new byte[] { '4',0, 0 };
        audio_LedPowerControl(power_on_data);
    }
}
